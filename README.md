# BACKTEST DE LA STRATEGIE DES TORTUES EN C#

## Objectif

L'objectif de cet exercice est de mettre en pratique les connaissances acquises tout au long du
cours sur le C# en développant une application de backtesting pour la stratégie de trading des
tortues. L'objectif est de créer une application console en C# qui chargera des données
historiques sur les prix des actifs financiers, appliquera la stratégie de trading des tortues, et
analysera la performance de cette stratégie au fil du temps.

## Contexte

La méthode des tortues a été créé par Richard Dennis et William Eckhard au siècle dernier. A
l'origine de cette stratégie, ces deux hommes cherchent à savoir si toutes personnes pouvait
devenir de bons traders, ou si les bons traders avaient une sorte de don intrinsèque. Ils décident
alors d'embaucher 13 traders et de leur apprendre la méthode des tortues. Chaque trader dispose
d'un compte de trading de plusieurs milliers de dollars (de 500'000 à 2'000'000 de dollars).
La méthode des tortues semble simple à première vue. Elle réunit tous les aspects du plan de
trading, du placement des ordres stop aux paramètres d'entrée en position. Le trader a des
consignes qu'il doit suivre machinalement. Si le trader ne suit pas à la lettre toutes les consignes,
la stratégie des tortues n'est alors plus efficaces.

## Tester le projet

```bash
git clone git@gitlab.com:ArToXxFR/tortues_csharp.git
cd tortues_csharp/EpsiTortues/
```

```bash
dotnet run
```

Choisir ensuite un code parmi les suivants

| Code | Nom de l'entreprise    |
|------|------------------------|
| MSFT | Microsoft Corportation |
| AAPL | Apple Inc.             |
| NVDA | Nvidia Corporation     |
| AMZN | Amazon.com, Inc.       |
| GOOG | Alphabet Inc.          |
| META | Meta Platforms, Inc.   |

Le backtest s'affiche maintenant dans la console

## Fonctionnement du backtest

### Architecture

Le code est composé de plusieurs classes : 

 - `Program`: Permet le déroulement du programme
 - `PriceManager`: Gère la récupération des données de prix depuis Yahoo Finance
 - `Asset`: Réprésente un actif financier avec ses données de prix
 - `TurtleStrategy`: La stratégie de backtest nommé "Turtle Strategy"

### Récupération des données de prix

PriceManager utilise HttpClient pour télécharger les données de Yahoo Finance.

Les données sont ensuite transformées en une liste d'objets AssetPrice.

### Exécution de la stratégie

La stratégie TurtleStrategy utilise les données de prix pour simuler des transactions et calculer la valeur finale du portefeuille.

### Affichage des résultats

La valeur finale du portefeuille et le nombre de jours utilisés sont affichés.

## Améliorations possibles

 - [ ] Ajout de nouvelles stratégies de backtest
 - [ ] Intégration de tests unitaires
 - [ ] Amélioration de la gestion des erreurs


