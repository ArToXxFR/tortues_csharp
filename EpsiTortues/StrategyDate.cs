﻿public class StrategyDate
{
    public DateTime Date { get; set; }
    public decimal ClosePrice { get; set; }
    public decimal HighPrice { get; set; }
    public decimal PreviousPortfolioValue { get; set; }
    public Position PreviousPosition { get; set; }
    public decimal PreviousPrice { get; set; }
    public decimal StopLoss { get; set; }
    public decimal MaximumPriceTrigger { get; set; }
    public Position CurrentPosition { get; private set; }
    public decimal CurrentPortfolioValue { get; private set; }
    public Position Position { get; private set; }
    public decimal TrailingStop { get; set; }

    public StrategyDate(AssetPrice price, decimal maximumPriceTrigger, Position previousPosition, decimal stopLoss, decimal highPrice, decimal previousPortfolioValue, decimal previousPrice)
    {
        Date = price.Date;
        ClosePrice = price.ClosePrice;
        HighPrice = highPrice;
        PreviousPortfolioValue = previousPortfolioValue;
        PreviousPosition = previousPosition;
        PreviousPrice = previousPrice;
        StopLoss = stopLoss;
        MaximumPriceTrigger = maximumPriceTrigger;
        CurrentPosition = GetCurrentPosition();
        CurrentPortfolioValue = GetCurrentPortfolioValue();
    }

    private Position GetCurrentPosition()
    {
        if (PreviousPosition == Position.Invested)
        {
            if (ClosePrice < PreviousPrice * (1 - StopLoss))
            {
                return Position.ExitPoint;
            }
        }
        else if (PreviousPosition == Position.ExitPoint)
        {
            if (ClosePrice > MaximumPriceTrigger)
            {
                return Position.NonInvested;
            }
        }
        else if (PreviousPosition == Position.NonInvested)
        {
            if (ClosePrice > PreviousPrice)
            {
                return Position.Invested;
            }
        }

        return PreviousPosition;
    }

    private decimal GetCurrentPortfolioValue()
    {
        if (CurrentPosition == Position.Invested)
        {
            return ClosePrice * 10000;
        }
        else if (CurrentPosition == Position.ExitPoint)
        {
            return PreviousPortfolioValue * (ClosePrice / PreviousPrice);
        }

        return PreviousPortfolioValue;
    }

    public bool GetBuySignal()
    {
        return PreviousPosition == Position.NonInvested && ClosePrice > PreviousPrice;
    }

    public bool GetSellSignal()
    {
        return CurrentPosition == Position.Invested && ClosePrice < PreviousPrice * (1 - StopLoss);
    }

    public void UpdateCurrentPortfolioValue(decimal price)
    {
        PreviousPortfolioValue = CurrentPortfolioValue;
        PreviousPrice = ClosePrice;
        CurrentPosition = GetCurrentPosition();
        CurrentPortfolioValue = GetCurrentPortfolioValue();
    }
}