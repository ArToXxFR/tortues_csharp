﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

public class PriceManager
{
    private static PriceManager? _instance;
    private readonly HttpClient _httpClient;
    private readonly IAssetPriceFactory _assetPriceFactory;

    private PriceManager(HttpClient httpClient, IAssetPriceFactory assetPriceFactory)
    {
        _httpClient = httpClient;
        _assetPriceFactory = assetPriceFactory;
    }

    public static PriceManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new PriceManager(new HttpClient(), new AssetPriceFactory());
            }

            return _instance;
        }
    }

    public async Task<List<AssetPrice>> GetPricesAsync(string assetCode)
    {
        try
        {
            var response = await _httpClient.GetAsync($"https://query1.finance.yahoo.com/v7/finance/download/{assetCode}?period1=1678613365&period2=1710235765&interval=1d&events=history&includeAdjustedClose=true");
            var content = await response.Content.ReadAsStringAsync();

            var prices = new List<AssetPrice>();
            var lines = content.Split('\n');

            foreach (var line in lines.Skip(1))
            {
                var values = line.Split(',');
                var date = DateTime.Parse(values[0]);
                var closePrice = decimal.Parse(values[4], System.Globalization.CultureInfo.InvariantCulture);

                prices.Add(_assetPriceFactory.Create(date, closePrice));
            }

            return prices;
        }
        catch (Exception ex)
        {
            throw new Exception($"Erreur lors de la récupération des données : {ex}", ex);
        }
    }
}

public class AssetPrice
{
    public DateTime Date { get; set; }
    public decimal ClosePrice { get; set; }
}

public interface IAssetPriceFactory
{
    AssetPrice Create(DateTime date, decimal closePrice);
}

public class AssetPriceFactory : IAssetPriceFactory
{
    public AssetPrice Create(DateTime date, decimal closePrice)
    {
        return new AssetPrice
        {
            Date = date,
            ClosePrice = closePrice
        };
    }
}

public class Asset
{
    public string? Code { get; set; }
    public List<AssetPrice>? Prices { get; set; }
    public decimal StrategyFinalValue { get; set; }

    public void RunStrategy(TurtleStrategy strategy)
    {
        if (Prices == null || Prices.Count < strategy._nbDays + 1)
        {
            throw new Exception("Pas assez de données pour la stratégie");
        }

        var pricesCopy = Prices.ToList();

        StrategyFinalValue = 100;
        decimal? highPrice = null;
        Position currentPosition = Position.NonInvested;

        for (int t = strategy._nbDays + 1; t < pricesCopy.Count; t++)
        {
            var maximumPriceTrigger = pricesCopy.Skip(t - strategy._nbDays).Take(strategy._nbDays).Max(price => price.ClosePrice);

            if (highPrice == null)
            {
                highPrice = pricesCopy[t].ClosePrice;
            }
            else
            {
                highPrice = Math.Max(highPrice.Value, pricesCopy[t].ClosePrice);
            }

            var strategyDate = new StrategyDate(
                pricesCopy[t],
                maximumPriceTrigger,
                currentPosition,
                strategy._stopLoss,
                highPrice.Value,
                StrategyFinalValue,
                pricesCopy[t - 1].ClosePrice
            );

            currentPosition = strategyDate.CurrentPosition;
            StrategyFinalValue = strategyDate.CurrentPortfolioValue;

            if (currentPosition == Position.ExitPoint || currentPosition == Position.NonInvested)
            {
                highPrice = null;
            }
        }
    }
}

