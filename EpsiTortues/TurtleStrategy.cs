﻿public enum Position
{
    Invested,
    NonInvested,
    ExitPoint
}

public class TurtleStrategy
{
    public readonly int _nbDays;
    public readonly decimal _stopLoss;
    public readonly List<StrategyDate> _dates;
    public Position Position { get; set; }
    public decimal CurrentPortfolioValue { get; set; }

    public TurtleStrategy(int nbDays = 20, decimal stopLoss = 5)
    {
        _nbDays = nbDays;
        _stopLoss = stopLoss;
        _dates = new List<StrategyDate>();
        Position = Position.NonInvested;
    }

    public void AddDate(AssetPrice price)
    {
        if (_dates.Count == 0)
        {
            var initialStrategyDate = new StrategyDate(price, decimal.MaxValue, Position.NonInvested, _stopLoss, 0, 0, 0);
            _dates.Add(initialStrategyDate);
            return;
        }

        var previousDate = _dates[_dates.Count - 1];
        var maximumPriceTrigger = _dates.Where(x => x.Date < price.Date).Select(x => x.ClosePrice).Max();

        var strategyDate = new StrategyDate(
            price,
            maximumPriceTrigger,
            previousDate.Position,
            _stopLoss,
            previousDate.HighPrice,
            previousDate.PreviousPortfolioValue,
            previousDate.PreviousPrice
        );

        _dates.Add(strategyDate);

        if (Position == Position.Invested)
        {
            strategyDate.TrailingStop = Math.Min(previousDate.TrailingStop, maximumPriceTrigger);
        }

        UpdateCurrentPortfolioValue(price);
    }

    public bool GetBuySignal(List<AssetPrice> prices)
    {
        if (prices.Count < _nbDays)
        {
            return false;
        }

        var closePrices = prices.Select(x => x.ClosePrice).ToList();
        var minPrice = closePrices.Min();
        var maxPrice = closePrices.Max();

        return prices[^1].ClosePrice > maxPrice;
    }

    public bool GetSellSignal(List<AssetPrice> prices)
    {
        if (Position != Position.Invested)
        {
            return false;
        }

        var strategyDate = _dates[^1];

        return prices[^1].ClosePrice < strategyDate.TrailingStop;
    }

    public void UpdateCurrentPortfolioValue(AssetPrice price)
    {
        if (Position == Position.Invested)
        {
            CurrentPortfolioValue = _dates[^1].PreviousPortfolioValue * price.ClosePrice / _dates[^1].PreviousPrice;
        }
        else
        {
            CurrentPortfolioValue = _dates[^1].PreviousPortfolioValue;
        }
    }
}
