﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static PriceManager;

class Program
{
    static async Task Main(string[] args)
    {
        string[] validCodes = { "MSFT", "AAPL", "NVDA", "AMZN", "GOOG", "META" };

        string? assetCode;

        Console.WriteLine("Saisissez le code de l'actif : ");

        assetCode = Console.ReadLine();

        if (string.IsNullOrEmpty(assetCode) || !validCodes.Contains(assetCode))
        {
            throw new ArgumentException("Le code saisi n'est pas valide.");
        }

        Console.WriteLine($"L'actif {assetCode} a été sélectionné.");

        var priceManager = PriceManager.Instance;

        var prices = await priceManager.GetPricesAsync(assetCode);

        var asset = new Asset();
        asset.Code = assetCode;
        asset.Prices = prices;

        asset.RunStrategy(new TurtleStrategy());

        Console.WriteLine($"Le portefeuille vaut {asset.StrategyFinalValue} euros pour un investissement initial de 100 euros.");
    }
}
